import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="client")
public class Client implements Serializable {
    @Id
    @Column(name="nif")
    private String dni;

    @Column(name="nom")
    private String name;

    @Column(name="corrupte")
    private boolean premium;

    public Client() {
    }

    public Client( String dni, String name, boolean premium) {
        this.dni = dni;
        this.name = name;
        this.premium = premium;

    }



    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }


    @Override
    public String toString() {
        return "Client{" +
                "dni=" + dni +
                ", name='" + name + '\'' +
                ", premium=" + premium +
                '}';
    }
}
