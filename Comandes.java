import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name="comandes")
public class Comandes implements Serializable {
    @Id
    @Column(name = "num_comanda")
    private int num_com;
    @Column(name = "preu_total")
    private double preu;
    @Column(name = "data")
    private Date date;
    @Column(name = "dni_client")
    private String dni_client;


    public Comandes(int num_com, double preu, Date date, String dni_client) {
        this.num_com = num_com;
        this.preu = preu;
        this.date = date;
        this.dni_client = dni_client;
    }

    public Comandes() {
    }

    public int getNum_com() {
        return num_com;
    }

    public void setNum_com(int num_com) {
        this.num_com = num_com;
    }

    public double getPreu() {
        return preu;
    }

    public void setPreu(double preu) {
        this.preu = preu;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public String getDni_client() {
        return dni_client;
    }

    public void setDni_client(String dni_client) {
        this.dni_client = dni_client;
    }

    @Override
    public String toString() {
        return "Comandes{" +
                "num_com=" + num_com +
                ", preu=" + preu +
                ", date=" + date +
                ", client=" + dni_client +
                '}';
    }
}