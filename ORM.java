import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.criteria.internal.expression.function.CurrentDateFunction;
import org.hibernate.service.ServiceRegistry;

public class ORM {
    private static ArrayList<Client> clientlist = new ArrayList<>();
    private static ArrayList<Comandes> comandalsit = new ArrayList<>();

    private static Session session;
    private static SessionFactory sessionFactory;

    static {
        try {

            sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Comandes.class).addAnnotatedClass(Client.class).buildSessionFactory();

        } catch (Exception e) {

        }

    }

    public static int menu() {
        System.out.println("Operacions:\n1.Borrar CLient\n2.Update client\n3.Mostrar client\n4.Alta nou client\n5.Nova comanda\n6.Mostrar comanda\n7.Generacio resum fact\n0.Sortir");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();

    }


    public static void añadirList() {
        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            clientlist.clear();
            clientlist.clear();
            List<Client> lstPolitics = session.createQuery("from Client").getResultList();
            for (Client p : lstPolitics) {
                clientlist.add(p);
            }
            List<Comandes> lstPolitics1 = session.createQuery("from Comandes").getResultList();
            for (Comandes p : lstPolitics1) {
                comandalsit.add(p);
            }
        } finally {
            session.close();
        }
    }

    public static void llistarClients() {

        for (int i = 0; i < clientlist.size(); i++) {
            System.out.println(clientlist.get(i));
        }

    }

    public static void llistarComandes(String nif) {
        for (int i = 0; i < clientlist.size(); i++) {
            if (nif.equals(clientlist.get(i).getDni())) {
                System.out.println(clientlist.get(i));
            }
        }
        for (int j = 0; j < comandalsit.size(); j++) {
            if (nif.equals(comandalsit.get(j).getDni_client())) {
                System.out.println(comandalsit.get(j));
            }
        }


    }

    public static void borrarClient() {
        llistarClients();

        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            Client client = null;
            Scanner sc = new Scanner(System.in);
            System.out.println("Escriu el nif del client a eliminar");
            String nif = sc.next();
            for (int i = 0; i < clientlist.size(); i++) {
                if (nif.equals(clientlist.get(i).getDni())) {
                    client = new Client(nif, clientlist.get(i).getName(), clientlist.get(i).isPremium());
                }
            }
            for (int i = 0; i < comandalsit.size(); i++) {
                if (nif.equals(comandalsit.get(i).getDni_client())) {
                    session.delete(comandalsit.get(i));
                }
            }
            session.delete(client);
            System.out.println("client eliminat");
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    public static void actualitzarClient(Client client) {
        Scanner sc = new Scanner(System.in);
        llistarClients();
        System.out.println("Escriu el nif del client a actualitzar");
        String nif = sc.next();
        client.setDni(nif);
        try {
            session = sessionFactory.openSession();

            session.beginTransaction();
            Client clientupdate = session.get(Client.class, client.getDni());
            clientupdate.setName(client.getName());
            clientupdate.setPremium(client.isPremium());
            System.out.println("client actualitzat");
            session.getTransaction().commit();
        } finally {
            session.close();
        }

    }

    public static void mostrarclient() {
        Scanner sc = new Scanner(System.in);
        llistarClients();
        System.out.println("text a introduir per cercar nom del client");
        String nom = sc.next();
        try {
            session = sessionFactory.openSession();
            session.beginTransaction();
            List<Client> lstPolitics = session.createQuery("from Client where nom LIKE \'%" + nom + "%\'").getResultList();
            session.getTransaction().commit();

            for (Client p : lstPolitics) {
                System.out.println(p);
            }
        } finally {
            session.close();
        }
    }

    public static void addClient(Client client) {
        try {
            session = sessionFactory.openSession();

            session.beginTransaction();
            session.save(client);
            System.out.println("client inserit");
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    public static void addComanda(Comandes comandes) {
        try {
            session = sessionFactory.openSession();

            session.beginTransaction();
            session.save(comandes);
            System.out.println("comanda inserida");

            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }


    public static void main(String[] args) throws ParseException {
        Scanner sc = new Scanner(System.in);
        int opcio = menu();
        añadirList();
        while (opcio != 0) {
            switch (opcio) {
                case 1:
                    borrarClient();
                    añadirList();
                    break;
                case 2:
                    System.out.println("Client a actualitzar\nNom");
                    String nom = sc.next();
                    System.out.println("Es vip (true or false)");
                    Boolean vip = sc.nextBoolean();
                    Client c1 = new Client(null, nom, vip);
                    actualitzarClient(c1);
                    añadirList();
                    break;
                case 3:
                    mostrarclient();
                    break;
                case 4:
                    System.out.println("Client a inserir\nNif del client");
                    String nif = sc.next();
                    System.out.println("Nom del client");
                    nom = sc.next();
                    System.out.println("Es vip (true or false)");
                    vip = sc.nextBoolean();
                    c1 = new Client(nif, nom, vip);
                    addClient(c1);
                    añadirList();

                    break;
                case 5:
                    System.out.println("Clients");
                    llistarClients();
                    System.out.println("Nif del client");
                    nif = sc.next();
                    System.out.println("Numº comanda");
                    int num_com = sc.nextInt();
                    System.out.println("Preu de la comanda XX,XX");
                    double preu = sc.nextDouble();
                    System.out.println("Data de la comanda (dd/MM/yyyy)");
                    String date = sc.next();
                    Date data = new SimpleDateFormat("dd/MM/yyyy").parse(date);
                    Comandes comandes = new Comandes(num_com, preu, data, nif);
                    addComanda(comandes);
                    añadirList();
                    break;
                case 6:
                    llistarClients();
                    System.out.println("Escriu el nif del client a buscar comandes");
                    nif = sc.next();
                    llistarComandes(nif);
                    break;
                case 7:
//                    System.out.println("Introdueix un mes valid:  ");
//                    int mes = sc.nextInt();
//                    System.out.println("Introdueix un any valid:  ");
//                    int any = sc.nextInt();
//                    System.out.println("Introdueix un dni:  ");
//                    int dni = sc.nextInt();
//
                    break;
            }

            opcio = menu();
        }
    }
}
